import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TariffCalculatorComponent } from './tariff-calculator/tariff-calculator.component';
import { TrackProductComponent } from './track-product/track-product.component';
import { MainPageComponent } from './main-page/main-page.component';
import { HelpComponent } from './help/help.component';
import { PrivateOfficeComponent } from './private-office/private-office.component';
import { PersonalAreaComponent } from './personal-area/personal-area.component';
import { OrdersComponent } from './orders/orders.component';

import { MatTableModule, MatSortModule } from '@angular/material';
import { AuthorizationComponent } from './authorization/authorization.component';
import { RegistrationComponent } from './registration/registration.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {path: '', component: MainPageComponent},
  {path: 'TariffCalculator', component: TariffCalculatorComponent},
  {path: 'TrackProduct', component: TrackProductComponent},
  {path: 'MainPage', component: MainPageComponent},
  {path: 'Help', component: HelpComponent},
  {path: 'PrivateOffice', component: PrivateOfficeComponent},
  {path: 'orders', component: OrdersComponent},
  {path: 'personal-area', component: PersonalAreaComponent},
  {path: 'authorization', component: AuthorizationComponent},
  {path: 'registration', component: RegistrationComponent}
];


@NgModule({
  declarations: [
    TariffCalculatorComponent,
    TrackProductComponent,
    HelpComponent,
    PrivateOfficeComponent,
    MainPageComponent,
    OrdersComponent,
    PersonalAreaComponent,
    AuthorizationComponent,
    RegistrationComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    MatTableModule,
    MatSortModule,
    FormsModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
