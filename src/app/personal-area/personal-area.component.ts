import { Component, OnInit } from '@angular/core';
import { PersonalArea } from '../models/personalArea';
import { PersonalAreaService } from '../services/personal-area.service';
@Component({
  selector: 'app-personal-area',
  templateUrl: './personal-area.component.html',
  styleUrls: ['./personal-area.component.css']
})
export class PersonalAreaComponent implements OnInit {

  personalArea: PersonalArea;
  
  constructor(private personalAreaService: PersonalAreaService) { }

  getPersonalArea(): void {
    this.personalAreaService.getPersonalArea()
      .subscribe(personalArea => this.personalArea = personalArea);
  }

  ngOnInit() {
    this.getPersonalArea();
  }

}
