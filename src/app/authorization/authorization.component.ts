import { Component, OnInit } from '@angular/core';
import { AuthorizationData } from '../models/authorizationData';
import { AuthorizationService } from '../services/authorization.service';

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.css']
})
export class AuthorizationComponent implements OnInit {

  authorizationData: AuthorizationData;

  constructor(private authorizationService: AuthorizationService) { }

  ngOnInit() {
  	this.authorizationData = new AuthorizationData();
  }

  submitForm() {
  	var regExpUsername = /^[A-Za-z0-9\-\_\.]+$/gi;
  	if (this.authorizationData.username.length < 6 ||
  		this.authorizationData.password.length < 6) {
  		alert('Длина логина и пароля должны быть не менее 6 символов');
  		return;
  	}
  	if (!regExpUsername.test(this.authorizationData.username)) {
  		alert('Логин может содержать только латинские буквы, цифры, тире, точку и знак подчеркивания');
  		return;
  	}
  	var result = this.authorizationService.sendAuthorizationData(this.authorizationData);
  	console.log(result);
  	if (result === null) {
  		alert('Неверный логин или пароль');
  	} else if (typeof(result) === 'object' && typeof(result.userId) === 'number') {
  		localStorage.setItem('currentUser', JSON.stringify({userId: result.userId, token: ''}));
  		location.href = '/orders';
  	}
  }

}
