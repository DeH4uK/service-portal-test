import { Order } from '../models/order';

export const ORDERS: Order[] = [
	{
		id: 1,
		userId: 1,
		userFullName: 'Иванов Иван',
		departureAddress: 'г. Воронеж, ул. Ленина, 100, кв. 123',
		destinationAddress: 'г. Москва, ул. Ленина, 10, кв. 321',
		itemsCount: 2
	},
	{
		id: 2,
		userId: 1,
		userFullName: 'Иванов Иван',
		departureAddress: 'г. Воронеж, ул. Ленина, 100, кв. 123',
		destinationAddress: 'г. Волгоград, ул. Ленина, 10, кв. 321',
		itemsCount: 1
	},
	{
		id: 3,
		userId: 2,
		userFullName: 'R2D2',
		departureAddress: 'г. Москва, ул. Ленина, 10, кв. 321',
		destinationAddress: 'г. Воронеж, ул. Ленина, 100, кв. 123',
		itemsCount: 3
	},
	{
		id: 4,
		userId: 3,
		userFullName: 'Test',
		departureAddress: 'г. Москва, ул. Ленина, 22, кв. 32',
		destinationAddress: 'г. Воронеж, ул. Ленина, 12, кв. 4',
		itemsCount: 1
	},
	{
		id: 5,
		userId: 4,
		userFullName: 'Test userId 4',
		departureAddress: 'г. Краснодар, ул. Мира, 12, кв. 76',
		destinationAddress: 'г. Воронеж, ул. Ленина, 12, кв. 43',
		itemsCount: 1
	},
	{
		id: 6,
		userId: 4,
		userFullName: 'Test userId 4',
		departureAddress: 'г. Краснодар, ул. Мира, 12, кв. 76',
		destinationAddress: 'г. Воронеж, ул. Ленина, 22, кв. 44',
		itemsCount: 1
	}
];
