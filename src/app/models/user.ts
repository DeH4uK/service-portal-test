export class User {
	userId: number;
	username: string;
	email: string;
	phone: string;
	password: string;
}