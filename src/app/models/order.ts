export class Order {
	id: number;
	userId: number;
	userFullName: string;
	departureAddress: string;
	destinationAddress: string;
	itemsCount: number;
}
