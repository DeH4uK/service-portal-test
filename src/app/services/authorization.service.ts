import { Injectable } from '@angular/core';
import { AuthorizationData } from '../models/authorizationData';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor() { }

  sendAuthorizationData(data: AuthorizationData): User {
  	var users = localStorage.getItem('users');
  	var usersList: User[];
	try {
		usersList = JSON.parse(users);
		if (typeof(usersList) !== 'object') {
			throw 'not object';
		}
	} catch(e) {
		return null;
	}
	
	for (var i = usersList.length; i--;) {
		if (usersList[i].username === data.username &&
			usersList[i].password === data.password) {
			return usersList[i];
		}
	}
	
    return null;
  }

}
