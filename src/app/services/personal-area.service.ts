import { Injectable } from '@angular/core';
import { PersonalArea } from '../models/personalArea';
import { PERS } from '../mocks/mock-personalArea ';
import { Observable, of } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class PersonalAreaService {

  constructor() { }

  getPersonalArea(): Observable<PersonalArea> {
	return of(PERS);
  }

}
