import { Injectable } from '@angular/core';
import { Order } from '../models/order';
import { ORDERS } from '../mocks/mock-orders';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

	constructor() { }

	getOrders(): Observable<Order[]> {
		return of(ORDERS);
	}
}
