import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor() { }

  getUsers(): Observable<User[]> {
	var users = localStorage.getItem('users');
	if (users !== null) {
		return JSON.parse(users);
	} else {
		return null;
	}
  }

  putUser(user: User): number {
  	var users = localStorage.getItem('users');
  	var usersList;
	try {
		usersList = JSON.parse(users);
		if (typeof(usersList) !== 'object') {
			throw 'usersList not object';
		}
		if (typeof(usersList[usersList.length - 1].userId) !== 'number') {
			throw 'userId not number';
		}
		user.userId = usersList[usersList.length - 1].userId + 1;
	} catch(e) {
		usersList = [];
		user.userId = 1;
	}
	usersList.push(user);
	localStorage.setItem('users', JSON.stringify(usersList));
    return user.userId;
  }

}
