import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { RegistrationService } from '../services/registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  newUser: User;
  users: User[];

  constructor(private registrationService: RegistrationService) { }

  ngOnInit() {
  	this.newUser = new User();
  }

  submitForm() {
  	var result = this.registrationService.putUser(this.newUser);
  	console.log(result);
  	console.log(this.registrationService.getUsers());
  }

}
