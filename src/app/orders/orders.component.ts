import { Component, OnInit, ViewChild } from '@angular/core';
import { Order } from '../models/order';
import { ORDERS } from '../mocks/mock-orders';
import { OrdersService } from '../services/orders.service';
import { MatSort, MatTableDataSource } from '@angular/material';
import { DataSource } from '@angular/cdk/table';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

	orders: Order[];
	ordersDataSource: MatTableDataSource<Order>;
	displayedColumns = [];
	@ViewChild(MatSort) sort: MatSort;

	constructor(private ordersService: OrdersService) { }

	getOrders(): void {
		this.ordersService.getOrders().subscribe(orders => this.orders = orders);
		this.ordersDataSource = new MatTableDataSource(this.orders);
	}

	ngOnInit() {
		this.getOrders();
		this.displayedColumns = ['id', 'userFullName', 'departureAddress', 'destinationAddress', 'itemsCount'];
		this.ordersDataSource.sort = this.sort;
	}

}
